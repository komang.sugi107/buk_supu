<?php
    require_once dirname(__FILE__)."/../../config/config.php";

    class Template{
        public $pageTitle = "Project";
        public $contentTitle;
        public $contentDescription;
        public $activePage;
        public $logo="";
        public $boxTitle;
        public $boxFooter;

        //Start Content
        public function startContent(){
          session_start();
          if(!isset($_SESSION['nim'])){
        		echo "<script>
        			alert('Silahkan Login Kembali');location.href='index.php';
        		</script>";
        	}
            $this->startHtml();
            echo $this->headerTemplate();
            echo $this->sidebarTemplate();
            echo $this->contentTemplate();
        }

        //End Content
        public function endContent(){
            echo $this->footerTemplate();
        }

        //Start HTML
        public function startHtml(){
            echo "

            <!DOCTYPE html>
            <html>
            <head>
                <meta charset='utf-8'>
                <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                <title>$this->pageTitle</title>

                <!-- Tell the browser to be responsive to screen width -->
                <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
                <!-- Bootstrap 3.3.7 -->
                <link rel='stylesheet' href='".MAIN_URL."/bower_components/bootstrap/dist/css/bootstrap.min.css'>
                <!-- Font Awesome -->
                <link rel='stylesheet' href='".MAIN_URL."/bower_components/font-awesome/css/font-awesome.min.css'>
                <!-- Ionicons -->
                <link rel='stylesheet' href='".MAIN_URL."/bower_components/Ionicons/css/ionicons.min.css'>
                <!-- Theme style -->
                <link rel='stylesheet' href='".MAIN_URL."/dist/css/AdminLTE.min.css'>
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                     folder instead of downloading all of them to reduce the load. -->
                <link rel='stylesheet' href='".MAIN_URL."/dist/css/skins/skin-green.min.css'>
                <!-- daterange picker -->
                <link rel='stylesheet' href='".MAIN_URL."/bower_components/bootstrap-daterangepicker/daterangepicker.css'>
                <!-- bootstrap datepicker -->
                <link rel='stylesheet' href='".MAIN_URL."/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'>
                <!-- iCheck for checkboxes and radio inputs -->
                <link rel='stylesheet' href='".MAIN_URL."/plugins/iCheck/all.css'>
                <!-- Bootstrap Color Picker -->
                <link rel='stylesheet' href='".MAIN_URL."/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css'>
                <!-- Bootstrap time Picker -->
                <link rel='stylesheet' href='".MAIN_URL."/plugins/timepicker/bootstrap-timepicker.min.css'>
                <!-- Select2 -->
                <link rel='stylesheet' href='".MAIN_URL."/bower_components/select2/dist/css/select2.min.css'>

                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                <script src='https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'></script>
                <script src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js'></script>
                <![endif]-->
                <!-- Google Font -->
                <link href='https://fonts.googleapis.com/css?family=Arimo' rel='stylesheet'>
                <style>
                    *{
                        font-family: 'Arimo', sans-serif;
                     }
                </style>
            </head>

            ";
        }

        public function endBody(){
            echo "</body>";
        }


        public function endHtml(){
            echo "</html>";
        }

        private function contentTemplate(){
            ob_start();
            include "content.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }

        private function headerTemplate() {
            ob_start();
            include "header.php";
            $header = ob_get_contents();
            ob_end_clean();

            return $header;
        }

        private function footerTemplate(){
            ob_start();
            include "footer.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }

        private function sidebarTemplate(){
            ob_start();
            include "sidebar.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }

        public function startBox(){
            echo $this->topBox();
        }

        public function conBox(){
            echo $this->contentBox();
        }

        public function endConBox(){
            echo $this->endcontentBox();
        }

        public function endBox(){
            echo $this->footerBox();
        }

        public function startModal(){
            echo $this->headerModal();
        }

        public function conModal(){
            echo $this->contentModal();
        }

        public function footModal(){
            echo $this->footerModal();
        }

        public function headalert(){
            echo $this->headeralert();
        }
        public function footalert(){
            echo $this->footeralert();
        }

        public function loader(){
            echo "<div id='preloader'>
                  <div id='loader'></div>";
        }

        private function topBox(){
            ob_start();
            include "start_box.php";
            $sbox = ob_get_contents();
            ob_end_clean();

            return $sbox;
        }

        private function contentBox(){
            ob_start();
            include "content_box.php";
            $sbox = ob_get_contents();
            ob_end_clean();

            return $sbox;
        }

        private function endcontentBox(){
            ob_start();
            include "end_content_box.php";
            $esbox = ob_get_contents();
            ob_end_clean();

            return $esbox;
        }


        private function footerBox(){
            ob_start();
            include "footer_box.php";
            $sbox = ob_get_contents();
            ob_end_clean();

            return $sbox;
        }

        private function headerModal(){
            ob_start();
            include "modal_header.php";
            $hm = ob_get_contents();
            ob_end_clean();

            return $hm;
        }

        private function contentModal(){
            ob_start();
            include "modal_content.php";
            $cm = ob_get_contents();
            ob_end_clean();

            return $cm;
        }

        private function footerModal(){
            ob_start();
            include "modal_footer.php";
            $mf = ob_get_contents();
            ob_end_clean();

            return $mf;
        }

        private function headeralert(){
            ob_start();
            include "alert_header.php";
            $ha = ob_get_contents();
            ob_end_clean();

            return $ha;
        }

        private function footeralert(){
            ob_start();
            include "alert_footer.php";
            $fa = ob_get_contents();
            ob_end_clean();

            return $fa;
        }

    }
?>
