<?php
	require_once dirname(__FILE__)."/components/template/main.php";
	require_once dirname(__FILE__)."/config/config.php";

	$template = new template();

	//Start HTML
    $template->pageTitle="Tugas Pemodelan";

    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Pemesanan Tiket Kuliah Industri";
    $template->startContent();

    function bulanIndo($bulan){
    	$bulanAngka = array('01','02','03','04','05','06','07','08','09','10','11','12');
		$namaBulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$convert = str_ireplace($bulanAngka, $namaBulan, $bulan);

		return $convert;
    }

?>

	<!-- Box -->
	<div class="row">
		<div class="col-md-12">
			<?php $template->startBox(); ?>
				<strong>Pesan Sekarang sebelum kehabisan</strong>
			<?php $template->conBox();?>
			<div class="col-md-12">
				<table class="table table-striped table-bordered">
					<thead class="thead">
						<tr>
							<td>No.</td>
							<td>Sesi</td>
							<td>Narasumber</td>
							<td>Harga</td>
							<td>Tanggal</td>
							<td>lokasi</td>
							<td>Jam</td>
							<td>Hari</td>
							<td>Stock</td>
							<td>Pilih</td>
						</tr>
					</thead>
					<tbody>
						<?php
							$no=0;
							$query = "SELECT * FROM jadwal";
							$execute = mysqli_query($connect, $query)or die(mysqli_error($connect));
							while($result = mysqli_fetch_assoc($execute)){
								$no++;
						?>
							<tr>
								<td><?= $no ?></td>
								<td align="center"><?= $result['sesi']; ?></td>
								<td><?= $result['narasumber']; ?></td>
								<td><?= $result['harga']; ?></td>	
								<?php
									$tanggal = $result['tanggal'];
									$search = explode('-',$tanggal);
									$tahun = $search[0];
									$bulans = $search[1];
									$hari = $search[2];

									$bulanx = $search[1];
									$cnvrt = bulanIndo($bulanx);
									$hasil = $hari.(" ").$cnvrt.(" ").$tahun;
								?>

								<td><?= $hasil ?></td>
								<td><?= $result['lokasi']; ?></td>
								<td><?= $result['jam']; ?></td>
								<td><?= $result['hari']; ?></td>
								<td><?= $result['stock']; ?></td>
								<td align="center">
									<a href="#myModal" data-toggle="modal" data-id="<?= $result['id']; ?>">
										<button class="btn btn-success">
											<span class="glyphicon glyphicon-edit"></span> Pilih
										</button>
									</a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
				<?php $template->endConBox();?>
				<?php $template->endBox();?>
		</div>
	</div>
<!-- End Content -->
<?php $template->endContent(); ?>

<?php
	$template->startModal();
 	$template->conModal();
	$template->footModal();
?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#myModal').on('show.bs.modal',function(e){
			var rowid = $(e.relatedTarget).data('id');

			$.ajax({
				type : 'post',
				url : "<?= MAIN_URL ?>/form/form.php",
				data : 'rowid='+rowid,
				success : function(data){
					$('.fetched-data').html(data);
				}

			});
		});
	});
</script>

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>
