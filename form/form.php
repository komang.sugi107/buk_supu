<?php
	require_once dirname(__FILE__)."/../components/template/main.php";

	$rowid = $_REQUEST['rowid'];
?>
<form method="post" action="action/simpanPembelian.php" class="form-horizontal"> 
	
	<input type="hidden" name="idKuliah" class="form-control" value="<?= $rowid ?>">

	<!-- Nama -->
	<div class="form-group">
		<label class="col-sm-4 control-label">Nama</label>

		<div class="col-sm-5">
			<input type="text" name="nama" class="form-control" required="required">
		</div>
	</div>

	<!-- Alamat -->
	<div class="form-group">
		<label class="col-sm-4 control-label">Alamat</label>

		<div class="col-sm-5">
			<input type="text" name="alamat" class="form-control" required="required">
		</div>
	</div>


	<!-- Program studi-->

	<div class="form-group">
		<label class="col-sm-4 control-label">Program Studi</label>

		<div class="col-sm-5">
			<select name="programStudi" class="form-control" required="required">
				<option value="">.......</option>
				<option value="1">Sistem Informasi</option>
				<option value="2">Sistem Komputer</option>
				<option value="3">Managemen Informatika</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-4 control-label">No Telp</label>

		<div class="col-sm-5">
			<input type="text" name="noTelp" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-4 control-label">Jumlah Tiket</label>

		<div class="col-sm-5">
			<input type="number" name="jumlah_tiket" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-4 control-label">Bayar Via </label>

		<div class="col-sm-5">
			<SELECT name="bayar" class="form-control" required>
				<option value="">.......</option>
				<?php
				 $bayar = mysqli_query($connect, "SELECT *FROM bayar") or die (mysqli_error($connect));
				 while($result= mysqli_fetch_array($bayar)){ ?>
				 	<option value="<?= $result['id'] ?>"> <?= $result['nama']?> </option>

				 <?php } ?>


				?>

				
			</SELECT>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-4 control-label"></label>

		<div class="col-sm-5">
			<input type="submit" name="submit" class="btn btn-success" value="OK">
		</div>
	</div>
</form>