<?php
	require_once dirname(__FILE__)."/../components/template/main.php";

	$rowid = $_REQUEST['rowid'];
	$query = "SELECT
				    pembayaran.kode_pembelian,
				    pembayaran.tanggal_beli,
				    jadwal.sesi,
				    jadwal.narasumber,
				    pembayaran.jumlah_tiket,
				    bayar.nama
				FROM
				    pembayaran
				JOIN jadwal ON pembayaran.id_kuliah = jadwal.id
				join bayar on pembayaran.id_bayar = bayar.id
				where pembayaran.id = '$rowid'";
	$execute = mysqli_query($connect, $query)or die(mysqli_error($connect));
	$result = mysqli_fetch_array($execute);

?>
<form method="post" action="action/simpanPembelian.php" class="form-horizontal"> 
	
	<input type="text" name="idKuliah" class="form-control" value="<?= $rowid ?>">

	<!-- Nama -->
	<div class="form-group">
		<label class="col-sm-4 control-label">Kode bayar</label>

		<div class="col-sm-5">
			<input type="text" name="kode_pembelian" class="form-control" required="required" value="<?= $result['kode_pembelian'] ?>">
		</div>
	</div>

	<!-- Alamat -->
	<div class="form-group">
		<label class="col-sm-4 control-label">Tanggal beli</label>

		<div class="col-sm-5">
			<input type="text" name="tanggal_beli" class="form-control" required="required" value="<?= $result['tanggal_beli'] ?>">
		</div>
	</div>


	<!-- Program studi-->

	<div class="form-group">
		<label class="col-sm-4 control-label">Sesi yang dipilih</label>

		<div class="col-sm-5">
			<select name="sesi" class="form-control" required="required">
				<option value="<?= $result['sesi'] ?>"><?= $result['sesi'] ?></option>
				<option value="">.......</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-4 control-label">Jumlah Tiket</label>

		<div class="col-sm-5">
			<input type="number" name="jumlah_tiket" class="form-control" value="<?= $result['jumlah_tiket'] ?>">
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-4 control-label">Metode Pembayaran</label>
		
		<div class="col-sm-5">
			<SELECT name="bayar" class="form-control" required>
				<option value="<?= $result['nama'] ?>"><?= $result['nama'] ?></option>
				<option value="">.......</option>
				<?php
				 $bayar = mysqli_query($connect, "SELECT *FROM bayar") or die (mysqli_error($connect));
				 while($result= mysqli_fetch_array($bayar)){ ?>
				 	<option value="<?= $result['id'] ?>"> <?= $result['nama']?> </option>

				 <?php } ?>


				?>

				
			</SELECT>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-4 control-label"></label>

		<div class="col-sm-5">
			<input type="submit" name="submit" class="btn btn-success" value="OK">
		</div>
	</div>
</form>