-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2018 at 09:49 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stikom`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `asal` varchar(60) NOT NULL,
  `tempat tinggal` varchar(50) NOT NULL,
  `no tlp` char(13) NOT NULL,
  `status` int(1) NOT NULL,
  `password` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `asal`, `tempat tinggal`, `no tlp`, `status`, `password`, `username`, `level`) VALUES
(987654321, 'leonard agastia', 'kota Denpasar', 'jl.Bung Tomo VII NO.5', '0816718092651', 1, 'c8499454bada15f6d76bbf8cf133960f93f9b4eb', 'leon', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id` int(11) NOT NULL,
  `sesi` char(1) DEFAULT NULL,
  `narasumber` varchar(60) DEFAULT NULL,
  `jam` time DEFAULT NULL,
  `hari` varchar(10) DEFAULT NULL,
  `lokasi` varchar(50) NOT NULL,
  `pilih` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `stock` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id`, `sesi`, `narasumber`, `jam`, `hari`, `lokasi`, `pilih`, `tanggal`, `stock`, `harga`) VALUES
(1, '1', 'Erlangga Maharesha \"GOJEK\"', '06:00:00', 'Kamis', 'Aula lt4', '', '2018-05-03', 150, 45000),
(2, '2', 'Erlangga Maharesha \"GOJEK\"', '08:00:00', 'Kamis', 'Aula lt4', '', '2018-05-03', 150, 45000),
(3, '3', 'Erlangga Maharesha \"GOJEK\"', '10:00:00', 'Kamis', 'Aula lt4', '', '2018-05-03', 150, 45000),
(4, '1', 'Putu Khrisna Adi P \"Payment Gateway\"', '12:00:00', 'Kamis', 'Ruang 4.1', '', '2018-05-03', 65, 45000),
(5, '2', 'Putu Khrisna Adi P \"Payment Gateway\"', '14:00:00', 'Kamis', 'Ruang 4.2', '', '2018-05-03', 65, 45000),
(6, '3', 'Putu Khrisna Adi P \"Payment Gateway\"', '16:00:00', 'Kamis', 'Ruang 4.3', '', '2018-05-03', 65, 45000),
(7, '1', 'JRX \"Musisi & Entrepreneur\"', '06:00:00', 'Jumat', 'Aula lt4', '', '2018-05-04', 150, 45000),
(8, '2', 'JRX \"Musisi & Entrepreneur\"', '08:00:00', 'Jumat', 'Aula lt4', '', '2018-05-04', 150, 45000),
(9, '3', 'JRX \"Musisi & Entrepreneur\"', '10:00:00', 'Jumat', 'Aula lt4', '', '2018-05-04', 150, 45000),
(10, '1', 'Hai Puja \"Puja Astawa\"', '12:00:00', 'Jumat', 'Ruang 4.4', '', '2018-05-04', 70, 45000),
(11, '2', 'Hai Puja \"Puja Astawa\"', '14:00:00', 'Jumat', 'Ruang 4.5', '', '2018-05-04', 70, 45000),
(12, '3', 'Hai Puja \"Puja Astawa\"', '16:00:00', 'Jumat', 'Ruang 4.6', '', '2018-05-04', 70, 45000),
(13, '1', 'A.A Indra Dwipayani \"Strategi  Mengembangkan UMKM\"', '06:00:00', 'Sabtu', 'Ruang 4.1', '', '2018-05-05', 65, 45000),
(14, '2', 'A.A Indra Dwipayani \"Strategi  Mengembangkan UMKM\"', '08:00:00', 'Sabtu', 'Ruang 4.2', '', '2018-05-05', 65, 45000),
(15, '3', 'A.A Indra Dwipayani \"Strategi  Mengembangkan UMKM\"', '10:00:00', 'Sabtu', 'Ruang 4.3', '', '2018-05-05', 65, 45000),
(16, '1', 'R.Arief Prawiro Utomo \"Teknologi Scada dan penerapannya\"', '12:00:00', 'Sabtu', 'Ruang 4.5', '', '2018-05-05', 65, 45000),
(17, '2', 'R.Arief Prawiro Utomo \"Teknologi Scada dan penerapannya\"', '14:00:00', 'Sabtu', 'Ruang 4.6', '', '2018-05-05', 65, 45000),
(18, '3', 'R.Arief Prawiro Utomo \"Teknologi Scada dan penerapannya\"', '16:00:00', 'Sabtu', 'Ruang 4.7', '', '2018-05-05', 65, 45000);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` char(9) NOT NULL,
  `nama` varchar(60) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `tanggalLahir` date DEFAULT NULL,
  `agama` char(1) DEFAULT NULL,
  `noTelp` char(15) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `namaAyah` varchar(60) DEFAULT NULL,
  `namaIbu` varchar(60) DEFAULT NULL,
  `alamatOrangtua` varchar(100) DEFAULT NULL,
  `jenisKelamin` char(1) DEFAULT NULL,
  `password` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama`, `alamat`, `tanggalLahir`, `agama`, `noTelp`, `status`, `namaAyah`, `namaIbu`, `alamatOrangtua`, `jenisKelamin`, `password`) VALUES
('160030200', 'Ditta', 'Denpasar', '1998-11-05', '1', '081234567890', '1', 'asd', 'asd', 'asd', '1', 'c8499454bada15f6d76bbf8cf133960f93f9b4eb'),
('160030237', 'evi', 'akdhfgaksf', '2018-05-01', '1', '24537975', '2', 'ehfhmfnscdas', 'asdfghjkl', 'qaxc', '1', '613484d4e063dc0221759f46fdbb0fe72323daca');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nim` char(9) NOT NULL,
  `idkuliah` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `harga` (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
