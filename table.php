<?php
	require_once dirname(__FILE__)."/../components/template/main.php";
	require_once dirname(__FILE__)."/../config/config.php";

	$template = new template();

	//Start HTML
    $template->pageTitle="Tugas Pemodelan";

    //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> <strong>Data pemesanan tiket kuliah industri</strong>";
    $template->startContent();

    function bulanIndo($bulan){
    	$bulanAngka = array('01','02','03','04','05','06','07','08','09','10','11','12');
		$namaBulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
		$convert = str_ireplace($bulanAngka, $namaBulan, $bulan);

		return $convert;
    }

?>

	<!-- Box -->
	<div class="row">
		<div class="col-md-12">
			<?php $template->startBox(); ?>
			<i> <strong></strong> </i>
			<?php $template->conBox();?>
			<div class="col-md-12">
				<table class="table table-striped table-bordered">
					<thead class="thead">
						<tr>
							<!-- data mahasiswa membeli tiket -->
							<th>No.</th>
							<th>Sesi</th>
							<th>Nim</th> <!-- terserah isi atau tidak -->
							<th>Narasumber</th>
							<th>Tanggal</th>
							<th>lokasi</th>
							<th>Jam</th>
							<th>Hari</th>
							<th>Keterangan</th>	<!--keterangan hapus data pembeli-->
							<th>Keterangan</th>
							<!--boleh ditambah lagi berapa space untuk menonton -->
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>asdasd</td>
						</tr>
					</tbody>
				</table>
			</div>
				<?php $template->endConBox();?>
				<?php $template->endBox();?>
		</div>
	</div>
<!-- End Content -->
<?php $template->endContent(); ?>

<?php
	$template->startModal();
 	$template->conModal();
	$template->footModal();
?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#myModal').on('show.bs.modal',function(e){
			var rowid = $(e.relatedTarget).data('id');

			$.ajax({
				type : 'post',
				url : "<?= MAIN_URL ?>/form/form.php",
				data : 'rowid='+rowid,
				success : function(data){
					$('.fetched-data').html(data);
				}

			});
		});
	});
</script>

<!-- End </body> -->
<?php $template->endBody(); ?>

<!-- End HTML -->
<?php $template->endHtml(); ?>
